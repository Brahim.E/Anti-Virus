#####################################################################
# Jeu Anti-Virus | v1 [Fonctionnel] | Dev_Ils | Trophées de NSI 2024 #
#####################################################################

Description:
------------
Anti-Virus est un jeu de réflexion captivant qui met au défi les compétences stratégiques des joueurs. Dans ce jeu vidéo programmé en Python, les joueurs doivent 
évacuer du plateau un virus informatique ayant contaminée un système en déplaçant stratégiquement les autres pièces. Les joueurs auront le choix entre plusieurs 
défis de difficulté croissante, garantissant une expérience stimulante à tous les niveaux de compétences !


Comment jouer:
--------------
Pour profiter de l'expérience "Anti-Virus", suivez ces étapes simples :
1. Ouvrez le fichier "main.py" et exécutez-le.
2. Choisissez le niveau souhaité.
3. Résolvez le puzzle en déplaçant les pièces de manière stratégique pour évacuer le virus.


Défis:
------
L'utilisateur doit manœuvrer les pièces à l'aide de la souris tout en respectant les contraintes du plateau de jeu. Certaines pièces 
constituent des obstacles immuables, ajoutant une couche supplémentaire de complexité. La résolution des niveaux nécessite la prise en 
compte de ces blocages, tandis que la possibilité d'effectuer des déplacements groupés ajoute une dimension stratégique, permettant à 
deux pièces en collision de se déplacer ensemble.

Contrôles:
----------
Les contrôles du jeu sont entièrement gérés à la souris, offrant une expérience intuitive et interactive.


Structure des fichiers:
-----------------------
- Fonctions.py : Contient les quelques fonctions essentiels au bon fonctionnement du jeu.

- Piece.py : Contient les classes de gestions des pièces, représentant les pièces du jeu.

- main.py : Fichier principal du jeu où le jeu Anti-Virus est initialisé et exécuté.


Note:
-----
Il est préférable de jouer sur un écran en 1920 x 1080 pour pouvoir afficher entièrement la fenêtre du jeu.


Crédits musiques:
-----

Musique des menus :

Music provided by HearWeGo
Artist: Liam Thomas
Title: No Time


Merci à Lilian GRENIER de s'être porté volontaire pour tester notre projet.
